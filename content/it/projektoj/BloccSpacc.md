+++
Title = "BloccSpacc"
Slug = "BloccSpacc"
canonicalUrl = "https://github.com/octospacc/BloccSpacc"
+++

{{< embed "https://hlb0.octt.eu.org/Misc/BloccSpacc/" app "width: 540px; height: 560px;" >}}

Tutte le informazioni e il codice sorgente sono disponibili su Git:

* https://gitlab.com/octospacc/BloccSpacc
* https://github.com/octospacc/BloccSpacc
* https://gitea.it/octospacc/BloccSpacc

## LEGGIMI originale

<blockquote>

{{< GetRemote "https://gitlab.com/octospacc/BloccSpacc/-/raw/main/README.md" markdown >}}

</blockquote>
