+++
Title = "WinDog"
Slug = "WinDog"
canonicalUrl = "https://github.com/octospacc/WinDog"
+++

* https://gitlab.com/octospacc/WinDog
* https://github.com/octospacc/WinDog
* https://gitea.it/octospacc/WinDog

{{< embed "https://windog.octt.eu.org/" window >}}

## LEGGIMI originale

<blockquote>

{{< GetRemote "https://gitlab.com/octospacc/WinDog/-/raw/main/README.md" markdown >}}

</blockquote>
